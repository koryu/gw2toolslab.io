{
  description = "gw2tools";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
        {
          packages.gw2tools = pkgs.mkYarnPackage rec {
            src = ./.;
            packageJSON = ./package.json;
            yarnLock = ./yarn.lock;

            buildPhase = ''
              cp ${packageJSON} ./package.json
              yarn build --outDir=$out
            '';

            dontInstall = true;
            dontFixup = true;

            # yarn2nix forces its own dist phase, so we can't just set doDist = false
            distPhase = "# do nothing";
          };

          defaultPackage = self.packages.${system}.gw2tools;

          devShell = pkgs.mkShell {
            inputsFrom = builtins.attrValues self.packages.${system};
          };
        }
      );
}
