import itertools
import json
import pathlib
import re

import requests

BASE_URL = 'https://api.guildwars2.com/v2/achievements'
BLOCKLIST = [
    # we don't care about this
    'Daily Completionist',

    # only used in backpack collection
    'Daily League Participator',

    # race event only
    'Daily Racer',
    'Daily Rolling Racer',

    # events and stuff
    'Daily Bonus Rewards',

    # living world maps
    'Daily Bloodstone',
    'Daily Ember Bay',
    'Daily Bitterfrost',
    'Daily Lake Doric',
    'Daily Draconis Mons',
    "Daily Siren's Landing",
    'Daily Domain of Istan',
    'Daily Sandswept Isles',
    'Daily Kourna',
    'Daily Jahai Bluffs',
    'Daily Thunderhead Peaks',
    'Daily Dragonfall',
    'Daily Eventful Dragonfall',
    'Daily Mistborn Mote Collector',
    'Daily Grothmar',

    # Bjora stuff
    'Daily Bjora Marches',
    'Daily Aberrant Hunter',
    'Daily Destroyer Hunter',
    'Daily Fallen Hunter',
    'Daily Icebrood Hunter',
    'Daily Trial of Koda',
    'Daily Legendary Hunter',
    'Daily Corrupted Spirit Cleansing',
    'Daily Kodan Helping Paw',
    'Daily Drakkar Victory',
    'Daily Raven Sanctum Blessing',
    'Daily Storms of Winter Completion',
    'Daily Puzzle Completer',
    'Daily Sons of Svanir Hunter',
    'Daily Essence Chest Looter',
    'Daily Idols of Jormag Completer',

    # more living world maps
    'Daily Drizzlewood Coast',
    'Daily Cold War Strike',

    # champions
    'Daily Dragon Responder',
    'Daily .* Dragon Response Mission',
    'Daily:? .* Missions',
    'Daily:? .* Donations',
    'Daily:? .* Enemies',
    'Daily:? .* Special',
]

DATA_DIR = pathlib.Path(__file__).parent / 'src' / 'lib' / 'data'


def get_known_ids():
    all_ids = set()

    for file in (
            'pve/dailies.json',
            'pvp/dailies.json',
            'wvw/dailies.json',
            'fractals/recommended_achievements.json'
    ):
        with (DATA_DIR / file).open() as fd:
            data = json.load(fd)
            all_ids.update(int(x) for x in data.keys() if x.isdigit())

    with (DATA_DIR / 'fractals/daily_achievements.json').open() as fd:
        data = json.load(fd)
        for v in data.values():
            all_ids.update(v)

    return all_ids


def chunks(iterable, n):
    it = iter(iterable)
    while True:
        chunk = tuple(itertools.islice(it, n))
        if not chunk:
            return
        yield chunk


def fetch_achievements(ids):
    data = requests.get(BASE_URL, params={'ids': ','.join(str(i) for i in ids)}).json()
    for item in data:
        if item['name'].startswith("Daily"):
            if not any(re.search(w, item['name']) for w in BLOCKLIST):
                yield item


def main():
    api_ids = set(requests.get(BASE_URL).json())
    known_ids = get_known_ids()
    missing_ids = api_ids - known_ids
    for chunk in chunks(missing_ids, 50):
        for row in fetch_achievements(chunk):
            print(f'{row["id"]} - {row["name"]}')


if __name__ == '__main__':
    main()
