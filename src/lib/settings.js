import { AccessLevel } from './dailyLoader'

const DisplayMode = Object.freeze({
  SHOW_ALL: 0,
  HIGHLIGHT_MINE: 1,
  ONLY_MINE: 2
})

const DailyGroup = Object.freeze({
  PVE: 0,
  FRACTALS: 1,
  PVP: 2,
  WVW: 3,
  STRIKES: 4
})

const Visibility = Object.freeze({
  VISIBLE: 0,
  GREYED_OUT: 1,
  HIDDEN: 2
})

const DEFAULT_SETTINGS = {
  version: 2,
  dark: true,
  displayMode: DisplayMode.SHOW_ALL,
  accountAccess: {
    hot: true,
    pof: true,
    eod: true,
    maxCharLevel: 80
  },
  categories: [
    { id: DailyGroup.PVE, enabled: true },
    { id: DailyGroup.FRACTALS, enabled: true },
    { id: DailyGroup.PVP, enabled: true },
    { id: DailyGroup.WVW, enabled: true },
    { id: DailyGroup.STRIKES, enabled: true }
  ]
}

const getSettings = function () {
  if (localStorage.settings) {
    let settings = JSON.parse(localStorage.settings)

    settings = { ...DEFAULT_SETTINGS, ...settings }

    if (settings.version === undefined) {
      // very old
      return DEFAULT_SETTINGS
    }

    if (settings.version === 1) {
      settings.accountAccess.eod = true
      settings.version = 2
    }

    return settings
  }

  return DEFAULT_SETTINGS
}

const saveSettings = function (settings) {
  localStorage.settings = JSON.stringify(settings)
}

const isRestrictionCompatible = function (settings, restriction) {
  const hasHot = settings.accountAccess.hot
  const hasPof = settings.accountAccess.pof
  const hasEod = settings.accountAccess.eod
  const maxCharLevel = settings.accountAccess.maxCharLevel

  const minDailyLevel = restriction.level.min
  const maxDailyLevel = restriction.level.max
  const access = restriction.accessLevel

  if (maxCharLevel < minDailyLevel) {
    return false
  }

  if (maxCharLevel > maxDailyLevel) {
    return false
  }

  if (access === AccessLevel.ALL_ACCESS) {
    return true
  }

  if (access === AccessLevel.HOT_ONLY) {
    return hasHot
  }

  if (access === AccessLevel.POF_ONLY) {
    return hasPof
  }

  if (access === AccessLevel.EOD_ONLY) {
    return hasEod
  }

  if (access === AccessLevel.NOT_HOT) {
    return !hasHot
  }

  if (access === AccessLevel.NOT_POF) {
    return !hasPof
  }

  if (access === AccessLevel.NOT_EOD) {
    return !hasEod
  }

  return true
}

const isCompatible = function (settings, daily) {
  if (!daily.restrictions) {
    return true
  }

  for (const restriction of daily.restrictions) {
    if (isRestrictionCompatible(settings, restriction)) {
      return true
    }
  }

  return false
}

const getDailyVisibility = function (settings, daily) {
  if (settings.displayMode === DisplayMode.SHOW_ALL) {
    return Visibility.VISIBLE
  }

  const compatible = isCompatible(settings, daily)

  if (compatible) {
    return Visibility.VISIBLE
  }

  if (settings.displayMode === DisplayMode.HIGHLIGHT_MINE) {
    return Visibility.GREYED_OUT
  }

  return Visibility.HIDDEN
}

export { DisplayMode, DailyGroup, Visibility, getSettings, saveSettings, getDailyVisibility, isCompatible }
